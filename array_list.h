#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include "list.h"

const int MAXSIZE = 100;

class ArrayList : public List
{
	protected:
	int* m_data;
	int m_size;

	public:
	ArrayList();
	~ArrayList();

	//Accessors
	int size() const;
	void print() const;
	bool find(const int num) const;

	//Mutators
	void insert(const int num);
	bool remove(const int num);

	//Only provided by ArrayList
	void sort();
};

#endif //ARRAYLIST_H
