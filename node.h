#ifndef NODE_H
#define NODE_H

class Node {
        private:
        int m_value;
        Node* m_next;

        public:
        Node();
        Node(const int value);

        int value() const;
        Node* next() const;
        void set_value(const int value);
        void set_next(Node* const next);
};

#endif //NODE_H
