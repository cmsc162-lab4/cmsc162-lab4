#include "array_list.h"
#include "linked_list.h"
#include <iostream>

using namespace std;

int vals[] = {10, 5, 4, 9, 3, 1};

int main()
{
#ifdef ARRAY_LIST
	ArrayList list;
#endif
#ifdef LINKED_LIST
	LinkedList list;
#endif
	for (int i=0; i < 6; ++i) {
		list.insert(vals[i]);
	}
	list.print();
	cout << endl;
	if (list.find(9)) {
		cout << "Found 9!" << endl;
	}	
	if (list.find(6)) {
		cout << "Uh oh.  Found 6!" << endl;
	}
	cout << endl;
	list.remove(9);
	if (list.find(9)) {
		cout << "H oh.  Found 9 after removing!" << endl;
	}	
	cout << endl;
	list.print();
	cout << endl;
	cout << "The size of the list is: " << list.size() << endl;
	cout << endl;

	#ifdef ARRAY_LIST
		list.sort();
		list.print();
		cout << endl;
	#endif
}
