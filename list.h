#ifndef LIST_H
#define LIST_H

class List
{
	public:
	//Accessors
	virtual int size() const = 0;
	virtual void print() const = 0;
	virtual bool find(const int num) const = 0;

	//Mutators
	virtual void insert(const int num) = 0;
	virtual bool remove(const int num) = 0;
};

#endif //LIST_H
