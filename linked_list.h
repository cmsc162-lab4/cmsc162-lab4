#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "node.h"
#include "list.h"

class LinkedList : public List
{
	protected:
	Node* m_head;

	public:
	LinkedList();
	~LinkedList();
	
	//Accessors
	int size() const;
	void print() const;
	bool find(const int num) const;

	//Mutators
	void insert(const int num);
	bool remove(const int num);
};

#endif //LINKED_LIST_H
